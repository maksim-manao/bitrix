<?php

use Fw\Core\Config;
use Fw\Core\Validator;

include("Fw/init.php");
    $page = $app->getPage();
    $page->setProperty("TITLE", "Main");
    $page->setProperty("HEAD", "Главная");
    $page->setProperty("FIO", "Максим");


    $app->header();

    $app->includeComponent("main:news", "general", ["main", ["Новость 3"]]);

    echo $valueValid = "max@lll.ru";

    $valid = new Validator('minLength', 5); 
    if($valid->exec($valueValid)){
        echo " не меньше 5 ";
    }else{
        echo " меньше 5 ";
    }


    $valid = new Validator( 
        'chain', 
        true, 
        [ 
            new Validator('minLength', 5), 
            new Validator('regexp', '/^[A-Za-z0-9]{0,}$/'), 
            new Validator('email') 
        ] 
    ); 
 
    var_dump($valid->exec($valueValid)); 

    $valid = new Validator('in', ['a', 'l']); 
    $valueValid = ['l'];
    var_dump($valid->exec($valueValid)); 


?>

<pre>



        ---- 18.11.2021 ----

    1) Добавлен getPage в Application
    2) Изменения в проверке на уникальность перед получением строк/ссылок
    3) FwCore теперь класс. Traits стал частью ядра
    4) DOCUMENT_ROOT отвязан от FWCORE
    5) Page теперь оставляет макросы
    6) Изменения в работе буфера
    7) Получение всех макросов=>значений теперь формируется по другому
    8) Префикс макросов теперь задается отдельно
    9) global теперь не используется
    10) Page::head() больше не принимает id
    11) Изменение в end() который заменяет макросы на значение


        ---- 17.11.2021 ----

    1) Singleton реализация вынесена в trait
    2) Добавлена константа, которая проверяется во всех файлах
    3) Создана структура шаблонов
    4) В Application внедрен буфер
    5) Создан Page. По шаблону Singleton. В этом классе реализованы методы, 
которые помогают управлять контентом страницы
    6) Page теперь инициализируется в конструкторе Appliction
    7) Файл /index.php теперь содержит изменения по проекту



        ---- 16.11.2021 ----

    1) Создан Git репозиторий
    2) Создана минимальная структура
    3) Всем классам задан namespace
    4) Обновлен routes
    5) Реализован класс Aplication на паттерне Singleton
    6) Добавлен класс Config который работает с файлом config
    7) Создан init в котором реализован авто загрузчик классов


</pre>

<?php 


$app->includeComponent("main:interface.form", "general", Config::get("formmain"));

$app->footer();