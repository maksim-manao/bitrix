<?php

use Fw\Core\Application;

session_start();
define("FWCORE", true);
define("ROOT_PATH", $_SERVER['DOCUMENT_ROOT']);

spl_autoload_register(function($classname){
    $base_dir = ROOT_PATH . "/";
    $file = $base_dir . str_replace('\\', '/', $classname) . '.php';
    if(!file_exists($classname)){
        require_once $file;
    }
});

$app = Application::getInstance();
$app->getPage()->addCss("/Fw/css/bootstrap/bootstrap.min.css");
$app->getPage()->addJs("/Fw/js/bootstrap/bootstrap.min.js");