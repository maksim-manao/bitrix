<?php
namespace Fw\Core;

use Fw\Core\Type\ParameterDictionary;

class Server
    extends ParameterDictionary
{
    function __construct($arServer){
        if(isset($arServer["DOCUMENT_ROOT"])){
            $arServer["DOCUMENT_ROOT"] = rtrim($arServer["DOCUMENT_ROOT"], "/\\");
        }
        parent::__construct($arServer);
    }
}