<?php
namespace Fw\Core\Type;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;
use Traversable;

class Dictionary
    implements IteratorAggregate, ArrayAccess, Countable
{
    protected $lists = [];
    public function __construct($lists = null){
        if(!is_null($lists)){
            $this->lists = $lists;
        }
    }
    function get($name){
        if(isset($this->lists[$name]) || array_key_exists($name, $this->lists)){
            return $this->lists[$name];
        }
        return null;
    }
    function set($name, $val = null){
        if(is_array($name)){
            $this->lists = $name;
        }else{
            return $this->lists[$name] = $val;
        }
    }
    function getLists(){
        return $this->lists;
    }
    function setLists($arr){
        $this->lists = $arr;
    }
    function clear(){
        $this->lists = [];
    }
    function isEmpty(){
        return empty($this->lists);
    }


    public function getIterator(): Traversable{
        return new ArrayIterator($this->lists);
    }
 
    public function count(): int {
        return (int) count($this->lists);
    }

    public function offsetExists($offset):bool{
        return isset($this->lists[$offset]);
    }
    public function offsetGet($offset){
        return $this->offsetExists($offset) ? $this->lists[$offset] : null;
    }
    public function offsetSet($offset, $value):void{
        if(is_null($offset)){
            $this->lists[] = $value;
        }else{
            $this->lists[$offset] = $value;
        }
    }
    public function offsetUnset($offset):void{
        unset($this->lists[$offset]);
    }
}