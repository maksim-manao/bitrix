<?php
namespace Fw\Core\Type;

use Exception;

class ParameterDictionary
    extends Dictionary
{
    protected $arLists = null;


    protected function setListsNoDemand($lists){
        if(is_null($this->arLists))
            $this->arLists = $this->lists;
        $this->lists = $lists;
    }
    function getRaw($name){
        if(is_null($this->arLists))
        {
            if(isset($this->lists[$name]) || array_key_exists($name, $this->lists))
                return $this->lists[$name];
        }
        else
        {
            if(isset($this->arLists[$name]) || array_key_exists($name, $this->arLists))
                return $this->arLists[$name];
        }

        return null;
    }
 
    function toArrayRaw(){
        return $this->arLists;
    }
    

    function offsetSet($offset, $val) : void {
        throw new Exception("Can not set readonly value");
    }
    function offsetUnset($offset) : void {
        throw new Exception("Can not unset readonly value");
    }
}