<?php
namespace Fw\Core;

class Validator{
    private $type = "";
    private $rule;
    private $validators = [];

    function __construct(string $type, $rule = null, $validators = []){
        $this->type = $type;
        $this->rule = $rule;
        $this->validators = $validators;
    }

    function exec($value){
        $fun =& $this->type;
        return $this->$fun($value);
    }

    private function chain($value){
        foreach ($this->validators as $valid) {
            if($valid->exec($value) != $this->rule){
                return false;
            }
        }
        return true;
    }
    private function in($values){
        if(!is_array($values)){
            $values = [$values];
        }
        foreach ($values as $val) {
            if(in_array($val,$this->rule)){
                return true;
            }
        }
        return false;
    }
    private function regexp($value){
        return preg_match($this->rule, $value);
    }
    private function email($value){
        $this->rule = "/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u";
        return $this->regexp($value);
     //   return filter_var($value, FILTER_VALIDATE_EMAIL);
    }
    private function phone($value){
        $this->rule = "/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/";
        return $this->regexp($value);
    }
    private function minLength($value){
        return strlen($value) >= $this->rule;
    }
    private function maxLength($value){
        return strlen($value) >= $this->rule;
    }
    private function max($value){
        return $value <= $this->rule;
    }
    private function min($value){
        return $value >= $this->rule;
    }
    private function range($value){
        return $this->rule['min'] >= $value && $value <= $this->rule['max'];
    }
    private function not($value){
        return (isset($value) && $value) ? true : false;
    }
}