<?php 
namespace Fw\Core;
use Fw\Core\Traits\Singleton;


final class Page{
    use Singleton;
    private $js = [],
            $css = [],
            $tags = [],
            $property = [];

    function addJs(string $src){
        $this->js[md5($src)] = $src;
    }
    function addCss(string $link){
        $this->css[md5($link)] = $link;
    }
    function addString(string $str){
        $this->tags[md5($str)] = $str;
    }

    function setProperty(string $id, $value){
        $this->property[$id] = $value;
    }
    function getProperty(string $id){
        return $this->property[$id];
    }

    function showProperty(string $id){
        echo $this->getMacros("PROPERTY_$id") . "\n";
    }
    function showHead(){
        echo 
         $this->getMacros("JS") . "\n" 
        .$this->getMacros("CSS") . "\n"
        .$this->getMacros("STR") . "\n";
    }
    
    
    private function getScripts(){
        $str = "";
        foreach ($this->js as $value) {
            $str .= "<script src=\"$value\"></script>\n";
        }
        return $str;
    }
    private function getCss(){
        $str = "";
        foreach ($this->css as $value) {
            $str .= "<link rel=\"stylesheet\" href=\"$value\">\n";
        }
        return $str;
    }
    private function getTags(){
        $str = "";
        foreach ($this->tags as $value) {
            $str .= "$value\n";
        }
        return $str;
    }
    function getAllReplace(){
        $mas = [];
        $mas[$this->getMacros("JS")] = $this->getScripts();
        $mas[$this->getMacros("CSS")] = $this->getCss();
        $mas[$this->getMacros("STR")] = $this->getTags();
        foreach ($this->property as $key => $val) {
            $mas[$this->getMacros("PROPERTY_$key")] = $val;
        }
        return $mas;
    }
    function getMacros($param)
    {
        return "#FW_{$param}_MAKROS#";
    }

}