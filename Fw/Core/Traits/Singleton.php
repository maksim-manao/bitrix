<?php
namespace Fw\Core\Traits;

trait Singleton{
    private static $instance = null;
    private function __construct(){

    }
    private function __clone(){

    }
    private function __wakeup(){

    }
    static function getInstance(){
        if(is_null(self::$instance)){
            self::$instance = new self();
        }
        return self::$instance;
    }
}