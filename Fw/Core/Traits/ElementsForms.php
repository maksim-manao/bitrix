<?php 
namespace Fw\Core\Traits;

use Exception;

trait ElementsForms{
    function __call($name, $arg){
        if(!preg_match("/\bget/", $name)){
            throw new Exception("Не метод get");
        }
        $pattern = "/[A-Z][a-z]*/";
        preg_match_all($pattern, $name, $matches);
        $param = array_shift($matches[0]);
        foreach ($matches[0] as $value) {
            $param .= "_$value";
        }
        $param = strtolower($param);
        if(!count($arg)){
            $params[$param] =& $this->params[$param];
        }else{
            $params = $arg[0];
        }
        return isset($params[$param]) ? $params[$param] : "";
    }
}