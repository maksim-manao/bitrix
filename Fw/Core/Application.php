<?php
namespace Fw\Core;
use Fw\Core\Traits\Singleton;

final class Application{
    use Singleton;

    private $__components = [];
    private $pager = null;
    private $template = null;
    private $request = null;
    private function __construct(){
        $this->pager = Page::getInstance();
        $this->template = ROOT_PATH . Config::get("templates/path") . "/" . Config::get("templates/name");
        $this->request = new Request($_REQUEST);
    }
    function getRequest(){
        return $this->request;
    }
    function getPage(){
        return $this->pager;
    }
    function header(){
        $this->start();
        include($this->template . "/header.php");
    }
    function footer(){
        include($this->template . "/footer.php");
        echo $this->end(ob_get_clean());
    }
    private function start(){
        ob_start();
    }
    private function end(string $buffer){
        $mas = $this->pager->getAllReplace();
        $keys = array_keys($mas);
        $buffer = str_replace($keys, $mas, $buffer);
        return $buffer;
    }
    function restart(){
        ob_clean();
    }

    function includeComponent($component, $template, $params){
        $elements = explode(":", $component);
        $str = $elements[0] . "/" . $elements[1];
        $path = Config::get("components"). "/$str";

        $var = "$path/class.php";
        if(!file_exists($var)){
            return;
        }
        if(!isset($this->__components[$elements[1]])){
            $pre = get_declared_classes();
            include($var);
            $aft = get_declared_classes();
            $this->__components[$elements[1]] = "";
            foreach (array_diff($aft, $pre) as $value) {
                if(get_parent_class($value) === "Fw\Core\Component\Base"){
                    $this->__components[$elements[1]] = $value;
                    break;
                }
            }
        }
        if($this->__components[$elements[1]]){
            $component = new $this->__components[$elements[1]]($elements[1], $elements[0], $template, $params);
            $component->executeComponent();
        }
    }

}