<?php
namespace Fw\Core;

class Config{
    private static $config = [];
    private static $isopen = true;
    private static function open(){
        if(self::$isopen){
            self::$isopen = false;
            self::$config = include_once("Fw/config.php");
        }
    }
    private function __construct(){

    }
    static function get(string $path){
        self::open();
        $arg = explode('/', $path);
        $result = self::$config;
        foreach ($arg as $value) {
            $result = $result[$value];
        }
        return $result;
    }

}