<?php
namespace Fw\Core\Component;

use Fw\Core\Config;

abstract class Base{
    public $result = [];
    public $id = "";
    public $params = [];
    public $template = null;
    public $__path = "";
    function __construct($id, $namespase, $template, $params){
        $path = "$namespase/$id";
        $this->__path = Config::get("components") . "/$path";
        $this->id = $id;
        $this->template = new Template($path, $template, $this);
        $this->params = $params;
    }
    function getParams(){
        return $this->params;
    }
    function getResult(){
        return $this->result;
    }
    abstract function executeComponent();
}