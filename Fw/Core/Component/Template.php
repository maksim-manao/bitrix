<?php
namespace Fw\Core\Component;

use Exception;
use Fw\Core\Application;
use Fw\Core\Config;

class Template{
    public $__path = "";
    public $__relativePath = "";
    public $id = "";
    private $component;
    function  __construct($path, $id, $component){
        $this->__path = Config::get("components") . "/$path/templates/$id";
        $this->__relativePath = "/Fw/components/$path/templates/$id";
        $this->id = $id;
        $this->component = $component;
    }
    function render($page = "template"){
        $path =& $this->__path;

        $result = $this->component->getResult();
        $params = $this->component->getParams();

        $var = "$path/result_modifier.php";
        if(file_exists($var)){
            include($var);
        }
        
        $var = "$path/$page.php";
        if(file_exists($var)){
            include($var);
        }else{
            throw new Exception("Нету основного шаблона компонента");
        }

        $var = "$path/component_epilog.php";
        if(file_exists($var)){
            include($var);
        }

        $FwPage = Application::getInstance()->getPage();
        $var = "/style.css";
        if(file_exists($path . $var)){
            $FwPage->addCss($this->__relativePath . $var);
        }
        $var = "/script.js";
        if(file_exists($path . $var)){
            $FwPage->addJs($this->__relativePath . $var);
        }
    }
}