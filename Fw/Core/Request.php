<?php
namespace Fw\Core;

use Fw\Core\Type\ParameterDictionary;

class Request
    extends ParameterDictionary
{
    protected $server = null;

    function __construct($request){
        parent::__construct($request);
        $this->server = new Server($_SERVER);
    }
    function getServer(){
        return $this->server;
    }
}