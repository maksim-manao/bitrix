<?php

Fw\Core\FwCore::isCore();

use Fw\Core\Application;

$app = Application::getInstance();
$request = $app->getRequest();
?>


<form class="container general-form <?= $result->getAdditionalClass()?>" 
    action="<?= $params['action']?>" 
    method="<?= $params['method']?>" 
    <?foreach (($result->getAttr())?:[] as $key => $value) {
            echo " $key=\"$value\" ";
    }?>
>
    <div class="row m-3 p-3">
        <?php foreach ($params['elements'] as $element) : ?>
                <div class="col-3 text-end mb-3">
                    <span><?= $element['title']?></span>
                </div>
                <div class="col-9 mb-3">
                    <?
                    $valueRequest = isset($request['submit'])
                            ? ['value' => isset($request[$element['name']]) ? $request[$element['name']] : ""]
                            : [];
                    $app->includeComponent(
                        "main/interface.form/elements:"
                            .$element['type']
                            .($result->getMultiple($element) ? ".multiple": ""), 
                        "general", 
                        array_merge($element, $valueRequest)
                    );?>
                </div>
        <?endforeach?>
        <div class="row justify-content-end">
            <div class="col-5">
                <input type="submit" name="submit" value="отправить">
            </div>
        </div>
    </div>
</form>