<?php

use Fw\Core\Component\Base;
use Fw\Core\Traits\ClassElementsForms;
use Fw\Core\Traits\ElementsForms;

class InputRadio extends Base{
    use ElementsForms;
    use ClassElementsForms;
}