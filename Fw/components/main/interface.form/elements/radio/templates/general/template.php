<?php
Fw\Core\FwCore::isCore();
?>

<input 
    class="<?= $result->getAdditionalClass()?>" 
    type="radio" 
    name="<?= $params['name']?>" 
    value="<?= $result->getDefault()?>" 
    <?foreach (($result->getAttr())?:[] as $key => $val) {
            echo " $key=\"$val\" ";
    }?>
    <?if(isset($params['value'])) : ?>
    <?= $result->getValue() == $result->getDefault() ? " checked " : ""?>
    <?else : ?>
    <?= $result->getChecked() ? " checked " : ""?>
    <?endif?>
><?php
