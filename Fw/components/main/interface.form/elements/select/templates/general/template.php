<?php
Fw\Core\FwCore::isCore();
?>


<select 
    class="<?= $result->getAdditionalClass()?>" 
    name="<?= $params['name']?>" 
    <?foreach (($result->getAttr())?:[] as $key => $val) {
            echo " $key=\"$val\" ";
    }?>
>

<?foreach ($params['list'] as $value) : ?>
    <option 
        value="<?= $result->getDefault($value)?>"
        class="<?= $result->getAdditionalClass($value)?>" 
        <?foreach (($result->getAttr($value))?:[] as $key => $val) {
                echo " $key=\"$val\" ";
        }?>
        <?= $result->getValue() == $value['default'] ? " selected " : ""?>
    ><?= $value['title']?></option>
<?endforeach?>
</select>