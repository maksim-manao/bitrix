<?php
Fw\Core\FwCore::isCore();

for ($i=1; $i <= $params['count']; $i++) : ?>
<input 
    class="<?= $result->getAdditionalClass()?>" 
    type="text" 
    name="<?= $params['name']?>[]" 
    value="<?if($result->getValue()) {
        echo $result->getValue()[$i-1];
    }else {
        echo $result->getDefault()."_$i";
    }?>"
    <?foreach (($result->getAttr())?:[] as $key => $value) {
            echo " $key=\"$value\" ";
    }?>
>
<?endfor?>