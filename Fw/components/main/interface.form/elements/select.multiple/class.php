<?php

use Fw\Core\Component\Base;
use Fw\Core\Traits\ClassElementsForms;
use Fw\Core\Traits\ElementsForms;

class SelectMultiple extends Base{
    use ElementsForms;
    use ClassElementsForms;
}