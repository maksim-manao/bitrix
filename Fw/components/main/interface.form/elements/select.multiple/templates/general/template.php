<?php
Fw\Core\FwCore::isCore();
?>


<select 
    class="<?= $result->getAdditionalClass()?>" 
    name="<?= $params['name']?>[]" 
    <?foreach (($result->getAttr())?:[] as $key => $value) {
            echo " $key=\"$value\" ";
    }?>
    multiple
>

<?foreach ($params['list'] as $value) : print_r($params);?>
    <option 
        value="<?= $result->getDefault($value)?>"
        class="<?= $result->getAdditionalClass($value)?>" 
        <?foreach (($result->getAttr($value))?:[] as $key => $val) {
                echo " $key=\"$val\" ";
        }?>
        <?if(isset($params['value'])) : ?>
        <?= in_array($result->getDefault($value), $result->getValue()?:[]) ? " selected " : ""?>
        <?else : ?>
        <?= $result->getSelected($value) ? " selected " : ""?>
        <?endif?>
    ><?= $value['title']?></option>
<?endforeach?>
</select>