<?php
Fw\Core\FwCore::isCore();
?>

<textarea 
    class="<?= $result->getAdditionalClass()?>" 
    name="<?= $params['name']?>" 
    <?foreach (($result->getAttr())?:[] as $key => $value) {
            echo " $key=\"$value\" ";
    }?>
    rows="<?= $params['rows']?>" 
    cols="<?= $params['cols']?>" 
><?=$result->getValue() ? : $result->getDefault()?></textarea>