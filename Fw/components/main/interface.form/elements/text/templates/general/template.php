<?php
Fw\Core\FwCore::isCore();
?>
<input 
    class="<?= $result->getAdditionalClass()?>" 
    type="text" 
    name="<?= $params['name']?>" 
    value="<?=$result->getValue() ? : $result->getDefault()?>"
    <?foreach (($result->getAttr())?:[] as $key => $value) {
            echo " $key=\"$value\" ";
    }?>
>