<?php

use Fw\Core\Component\Base;

class News extends Base{

    function executeComponent(){
        $this->result = $this->params[1];
        $this->template->render();
    }
}