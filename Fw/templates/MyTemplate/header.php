<?php
Fw\Core\FwCore::isCore();
use Fw\Core\Application;
use Fw\Core\Config;

$page = Application::getInstance()->getPage();
    $page->setProperty("LOGOSRC", "");
    $page->setProperty("LOGOALT", "logo");
    $page->addCss(Config::get("templates/path") . "/MyTemplate/css/style.css");
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <?= $page->showHead()?>
    <title><?= $page->showProperty("TITLE")?></title>
</head>
<body>
    <div class="navbar position-sticky top-0">
        <div class="container col-9">
            <div class="col-3">
                <img src="<?= $page->showProperty("LOGOSRC")?>" alt="<?= $page->showProperty("LOGOALT")?>">
            </div>
            <div class="col-6 text-center">
                <span>Сегодня <?= date(DATE_ATOM)?></span>
            </div>
            <div class="col-3 text-end">
                <span><?= $page->showProperty("FIO");?></span>
            </div>
        </div>
    </div>
    <header class="main-header justify-content-center">
        <div class="container col-9">
            <div class="row">
                <div class="col">
                    <h2><?= $page->showProperty("HEAD")?></h2>
                </div>
            </div>
        </div>
    </header>
    <main class="main-content justify-content-center">
        <div class="container col-9">
