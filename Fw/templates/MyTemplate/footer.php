<?php

Fw\Core\FwCore::isCore();
use Fw\Core\Application;

?>

        </div>
    </main>
    <footer class="main-footer justify-content-center">
        <div class="container col-9">
            <div class="row">
                <p>footer</p>
                <?Application::getInstance()->includeComponent(
                    "main:news",
                    "general",
                    [
                        "all news",
                        [
                            "Новость 1",
                            "Новость 2",
                            "Новость 3"
                        ]
                    ]
                );?>
            </div>
        </div>
    </footer>
    </body>
</html>