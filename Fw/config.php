<?php
Fw\Core\FwCore::isCore();
$config = [
    "db" => [
        "login" => "123",
        "pass" => "asd"
    ],
    "templates" => [
        "name" => "MyTemplate",
        "path" => "/Fw/templates"
    ],
    "components" => ROOT_PATH . "/Fw/components",
    "formmain" => [ 
        'additional_class' => 'window--full-form', //доп класс на контейнер формы 
        'attr' => [  // доп атрибуты 
            'data-form-id' => 'form-123' 
        ], 
        'method' => 'post', 
        'action' => '', //url отправки 
        'elements' => [  //список элементов формы 
            [ 
                'type' => 'text', 
                'name' => 'login2', 
                'additional_class' => 'js-login col-7', 
                'attr' => [ 
                    'data-id' => '3' 
                ], 
                'title' => 'params', 
                'default' => 'param',
                'multiple' => true,
                'count' => 4
            ],
            [ 
                'type' => 'text', 
                'name' => 'login', 
                'additional_class' => 'js-login', 
                'attr' => [ 
                    'data-id' => '17' 
                ], 
                'title' => 'Логин', 
                'default' => 'Введите имя' 
            ], 
            [ 
                'type' => 'password', 
                'name' => 'password', 
                'title' => 'пароль' 
            ], 
            [ 
                'type' => 'select', 
                'name' => 'server', 
                'additional_class' => 'js-login', 
                'attr' => [ 
                    'data-id' => '17' 
                ], 
                'title' => 'Выберите сервер', 
                'default' => 'tut' ,
                'list' => [ 
                    [ 
                        'title' => 'Онлайнер', 
                        'default' => 'onliner', 
                        'additional_class' => 'mini--option', 
                        'attr' => [ 
                            'data-id' => '188' 
                        ]
                    ], 
                    [ 
                        'title' => 'Тутбай', 
                        'default' => 'tut'
                    ] 
                ] 
            ],
            [ 
                'type' => 'select', 
                'name' => 'server2', 
                'additional_class' => 'js-login', 
                'attr' => [ 
                    'data-id' => '17' 
                ], 
                'title' => 'Выберите сервер', 
                'multiple' => true,
                'list' => [ 
                    [ 
                        'title' => 'Онлайнер', 
                        'default' => 'onliner', 
                        'additional_class' => 'mini--option', 
                        'attr' => [ 
                            'data-id' => '188' 
                        ],
                        'selected' => true
                    ], 
                    [ 
                        'title' => 'Тутбай', 
                        'default' => 'tut',
                        'selected' => true
                    ] 
                ] 
            ], 
            [ 
                'type' => 'checkbox', 
                'name' => 'check', 
                'additional_class' => 'js-login', 
                'attr' => [ 
                    'data-id' => '17' 
                ], 
                'title' => 'подтверждение',
                'default' => 'zero'
            ],
            [ 
                'type' => 'checkbox', 
                'name' => 'check2', 
                'additional_class' => 'js-login', 
                'attr' => [ 
                    'data-id' => '17' 
                ], 
                'title' => 'подтверждение мульти',
                'default' => 'one',
                'multiple' => true
            ],
            [ 
                'type' => 'checkbox', 
                'name' => 'check2', 
                'additional_class' => 'js-login', 
                'attr' => [ 
                    'data-id' => '17' 
                ], 
                'title' => 'подтверждение мульти',
                'default' => 'two',
                'multiple' => true,
                'checked' => true
            ],
            [ 
                'type' => 'checkbox', 
                'name' => 'check2', 
                'additional_class' => 'js-login', 
                'attr' => [ 
                    'data-id' => '17' 
                ], 
                'title' => 'подтверждение мульти',
                'default' => 'three',
                'multiple' => true,
                'checked' => true
            ],
            [
                'type' => 'radio', 
                'name' => 'radio', 
                'additional_class' => 'js-login', 
                'attr' => [ 
                    'data-id' => '345' 
                ], 
                'title' => 'один',
                'default' => 'one'
            ],
            [
                'type' => 'radio', 
                'name' => 'radio', 
                'additional_class' => 'js-login', 
                'attr' => [ 
                    'data-id' => '33345' 
                ], 
                'title' => 'два' ,
                'default' => 'two',
                'checked' => true
            ],
            [
                'type' => 'radio', 
                'name' => 'radio',
                'additional_class' => 'js-login', 
                'attr' => [ 
                    'data-id' => '22' 
                ], 
                'title' => 'три' ,
                'default' => 'three',
            ]
        ] 
    ]
];
return $config;